---
title: IDA
description: IDA Cheatsheet
---

# Cheatsheet IDA

## Transform

- `c`: Tranform to code
- `d`: Transform to data
- `p`: Create a function
- `Alt+a`: Change encoding of a string (i.e. from UTF8 to UTF16-LE)

## Identify

- `n`: Rename a symbol
- `y`: Change type of the symbol
- `m`: Assign a *Named constant* to a variable
- `t`: Assign a *Structure field* to a variable
- `:`: Comments
- `;`: Repeatable comments (comments also in Xrefs)

## Structures

- `Ins`: New struct
- `d`: Add a new byte field
- `a`: Add a new array field

## Enums

- `Ins`: New enumeration
- `n`: Add enum member
- `CTRL+n`: Edit enum member

## Others

- `CTRL+w`: Save

## Search

- `Immediate value`: Searches for value inside operands and data.
- `Next code`: Search for an instruction.
- `Sequence of bytes`: Search for bytes in hex view.

## Windows

- Functions, Strings, Imports/Exports, Structure
- Names (list every address with a name)

## Graphs

- Flow chart
- Functions calls
- Cross-references for a specific symbol
- Cross-references to reach a specific symbol

## Navigation band

- **Light blue**: Code recognized by FLIRT signature.
- **Dark blue**: User-writtend code.
- **Red**: Compiler-generated code.

## Scripting language

### IDAPython

- **IDAPython:** Python integration to IDA.

### IDC

- **IDC:** IDA's C-like scripting language.
- **Load script:** File -> Script file...

#### Find XOR with different registers

```c
#include <idc.idc>

static main() {
    auto ea, next_ea;
    auto insn, op1, op2;

    // Iterate over all segments
	Message("Searching for XOR...\n");
    ea = FirstSeg();
    while (ea != BADADDR) {
        next_ea = NextSeg(ea);

        // Iterate over all instructions in the segment
        while (ea != BADADDR && ea < next_ea) {
            insn = GetMnem(ea);

            // Check if the instruction is XOR
            if (insn == "xor") {
                op1 = GetOpnd(ea, 0);
                op2 = GetOpnd(ea, 1);

                // Check if operands are different registers
                if (op1 != op2) {
                    Message("=> %a: %s, %s\n", ea, op1, op2);
                }
            }

            // Move to the next instruction
            ea = NextHead(ea, next_ea);
        }

        // Move to the next segment
        ea = next_ea;
    }
	Message("Done!\n");
}
```