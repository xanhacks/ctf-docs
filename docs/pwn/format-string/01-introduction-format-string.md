---
title: Introduction - Format string
description: Introduction to binary exploitation with format string.
---

# Introduction - Format string

## Format specifiers

- `%d` or `%i`: Argument will be used as decimal integer (signed or unsigned)
- `%o`: An octal unsigned integer
- `%u`: An unsigned decimal integer - this means negative numbers will wrap around
- `%x` or `%X`: An unsigned hexadecimal integer
- `%f`,  `%g`  or  `%G`: A floating-point number. %f defaults to 6 places after the decimal point (which is locale-dependent - e.g. in de_DE it will be a ,). %g and %G will trim trailing zeroes and switch to scientific notation (like %e) if the numbers get small or large enough.
- `%e` or `%E`: A floating-point number in scientific (XXXeYY) notation
- `%s`: A string
- `%b`: As a string, interpreting backslash escapes, except that octal escapes are of the form 0 or 0ooo.
- `%n`: Write the number of characters printed thus far to an `int` variable

## Vulernable functions

- `fprint`: Writes the printf to a file
- `printf`: Output a formatted string
- `sprintf`: Prints into a string
- `snprintf`: Prints into a string checking the length
- `vfprintf`: Prints the a va_arg structure to a file
- `vprintf`: Prints the va_arg structure to stdout
- `vsprintf`: Prints the va_arg to a string
- `vsnprintf`: Prints the va_arg to a string checking the length