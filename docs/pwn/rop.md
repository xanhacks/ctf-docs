---
title: ROP
description: ROP cheatsheet
---

# ROP

## EAX without gadgets

Control `eax` (without `eax` gadgets) using `syscalls`:

- [read](https://man7.org/linux/man-pages/man2/read.2.html): Number of bytes read.
- [write](https://man7.org/linux/man-pages/man2/write.2.html): Number of bytes written.
- [umask](https://man7.org/linux/man-pages/man2/umask.2.html): Returns the value of the mask.
- [alarm](https://man7.org/linux/man-pages/man2/alarm.2.html): Seconds between two alarms.
